const mix = require('laravel-mix');
mix.setPublicPath('./static')
mix
    .sass('./assets/scss/app.scss', './static/css')
    .js('./assets/js/app.js', './static/js')
    .version()